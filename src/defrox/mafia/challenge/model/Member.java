package defrox.mafia.challenge.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Model of a family member
 * 
 * @author defrox
 */
public class Member {

	/**
	 * Name of the member
	 * */
	private String name;

	/**
	 * Alias of the member
	 * */
	private String alias;

	/**
	 * Date when he/she joined the family
	 * */
	private Date joined;

	/** Member's boss */
	private Member boss;

	/** List of henchmen */
	private List<Member> henchmen;

	/**
	 * Class Constructor: Creates the member
	 * 
	 * @param name
	 * @param alias
	 * @param boss
	 */
	public Member(String name, String alias, Member boss) {
		this.name = name;
		this.alias = alias;
		this.boss = boss;
		this.joined = new Date();
		this.henchmen = new ArrayList<Member>();
	}

	/**
	 * Returns the name of the current member
	 *
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the current member
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the alias of the current member
	 *
	 * @return
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the alias of the current member
	 *
	 * @param alias
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Returns the date the member joined the family
	 *
	 * @return
	 */
	public Date getJoined() {
		return joined;
	}

	/**
	 * Sets the date the member joined the family
	 *
	 * @param joined
	 */
	public void setJoined(Date joined) {
		this.joined = joined;
	}

	/**
	 * Returns the boss of the current member
	 *
	 * @return
	 */
	public Member getBoss() {
		return boss;
	}

	/**
	 * Sets the boss of the current member
	 *
	 * @param boss
	 */
	public void setBoss(Member boss) {
		this.boss = boss;
	}

	/**
	 * Returns the list of henchmen for the current member
	 *
	 * @return
	 */
	public List<Member> getHenchmen() {
		return henchmen;
	}

	/**
	 * Sets the list of henchmen for the current member
	 *
	 * @param henchmen
	 */
	public void setHenchmen(List<Member> henchmen) {
		this.henchmen = henchmen;
	}
	
	/**
	 * Returns the member's full name and alias
	 *
	 * @return
	 */
	public String getFullNameAndAlias(){
		return getName() + " a.k.a " + getAlias();
	}
	
	/**
	 * Returns whether a member should be put under special surveillance
	 *
	 * @return
	 */
	public boolean hasSpecialSurveillance(){
		boolean result = false;
		if(this.getHenchmen()!=null && this.getHenchmen().size()>50){
			result = true;
		}
		return result;
	}
	
	/**
	 * Current member's info
	 *
	 * @return
	 */
	public String membersInfo(){
		StringBuffer buffer = new StringBuffer("");
		buffer.append("Member ");
		buffer.append(this.getFullNameAndAlias());
		buffer.append(" \rBoss: ");
		if(this.getBoss()!=null){
			buffer.append(this.getBoss().getFullNameAndAlias());
		}else{
			buffer.append(" NOT. HE IS THE GODFATHER");
		}
		
		buffer.append(" \rJoined: ");
		buffer.append(this.getJoined());
		
		List<Member> henchmen = this.getHenchmen();
		if(henchmen!=null && !henchmen.isEmpty()){
			buffer.append(" \rHenchmen: ");
			for(Member item: henchmen){
				buffer.append("\r      ");
				buffer.append(item.getFullNameAndAlias());
			}
		}
		
		buffer.append("\r---------------------------------------------");
		
		return buffer.membersInfo();
	}


	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		Member other = (Member) obj;
		if(other.getName().equalsIgnoreCase(this.getName())){
			result = true;
		}

		return result;
	}

}
