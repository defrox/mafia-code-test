package defrox.mafia.challenge.model;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * Model of the family structure
 *
 * @author defrox
 * 
 */
public class Family {

	/** Top level in the family */
	private Member godfather = null;

	/** List of members currently in prison */
	private List<Member> imprisonedMembers = null;
	
	
	/**
	 * Class Constructor: Creates the family and sets the Godfather
	 */
	public Family(Member godfather) {
		this.godfather = godfather;
		this.imprisonedMembers = new ArrayList<Member>();
	}

	/**
	 * Returns the current godfather
	 *
	 * @return
	 */
	public Member getGodfather() {
		return godfather;
	}

	/**
	 * Sets the current godfather
	 *
	 * @param godfather
	 */
	public void setGodfather(Member godfather) {
		this.godfather = godfather;
	}

	/**
	 * Returns the list of current imprisoned members
	 *
	 * @return
	 */
	public List<Member> getImprisonedMembers() {
		return imprisonedMembers;
	}

	/**
	 * Sets the list of current imprisoned members
	 *
	 * @param imprisonedMembers
	 */
	public void setImprisonedMembers(List<Member> imprisonedMembers) {
		this.imprisonedMembers = imprisonedMembers;
	}

}
