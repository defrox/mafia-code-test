package defrox.mafia.challenge;

import defrox.mafia.challenge.model.Member;
import defrox.mafia.challenge.model.Family;

import java.util.List;

/**
 * Mafia: main class
 *
 * @author defrox
 */
public class Mafia {


    private Family family = null;

    /**
     * Class Constructor: Creates the family registry
     */
    public Mafia() {
        family = new Family(null);
    }

    /**
     * Class Constructor: Creates the family registry and sets the godfather
     *
     * @param godfather
     */
    public Mafia(Member godfather) {
        family = new Family(godfather);
    }

    /**
     * Adds a new member to the family
     *
     * @param name
     * @param alias
     * @param boss
     * @return
     */
    public Member addMember(String name, String alias, Member boss) {
        Member item = new Member(name, alias, boss);
        addHenchman(boss, item);
        return item;
    }

    /**
     * Adds the new Godfather to the family
     *
     * @param name
     * @param alias
     * @return
     */
    public Member addMember(String name, String alias) {
        Member item = new Member(name, alias, null);
        family.setGodfather(item);
        return item;
    }

    /**
     * Sends a family member to prison.
     *
     * @param member
     */
    public void sendMemberToPrison(Member member) {

        // Add convict to list of imprisoned members
        List<Member> list = this.family.getImprisonedMembers();
        list.add(member);

        // Remove from henchmen list if is not a boss
        List<Member> sameLevelMembers = null;
        Member newBossForHenchmen = null;
        Member boss = member.getBoss();
        if (boss != null) {
            sameLevelMembers = boss.getHenchmen();
            sameLevelMembers.remove(member);

            if (sameLevelMembers.size() > 0) {
                newBossForHenchmen = sameLevelMembers.get(0);
            }
        }

        // If he has henchmen, they must get a new boss
        List<Member> henchmen = member.getHenchmen();
        if (henchmen != null && !henchmen.isEmpty()) {
            // There is a new boss for the henchmen
            if (newBossForHenchmen != null) {
                for (Member item : henchmen) {
                    // Sets the new boss
                    addHenchman(newBossForHenchmen, item);
                    item.setBoss(newBossForHenchmen);
                }

                // The oldest henchman has to promote
            } else {
                Member first = henchmen.get(0);
                henchmen.remove(first);
                if (boss != null) {
                    addHenchman(boss, first);
                    first.setBoss(boss);
                } else {
                    // We have a new godfather
                    first.setBoss(null);
                    this.family.setGodfather(first);
                }

                // Change the structure
                for (Member item : henchmen) {
                    addHenchman(first, item);
                    item.setBoss(first);
                }
            }
        }
    }

    /**
     * Releases a family member from prision
     *
     * @param member
     */
    public void releaseMemberFromPrison(Member member) {

        // Give him back his position by setting his henchmen
        List<Member> henchmen = member.getHenchmen();
        if (henchmen != null) {
            for (Member henchman : henchmen) {
                List<Member> colleagues = henchman.getBoss().getHenchmen();
                colleagues.remove(henchman);
                henchman.setBoss(member);
            }
        }

        // Set his boss
        Member boss = member.getBoss();
        // The godfather doesn't have a boss
        if (boss != null) {
            addHenchman(boss, member);
        }

        // Remove him from the list of convicts
        List<Member> list = this.family.getImprisonedMembers();
        list.remove(member);

    }

    /**
     * Returns whether a member should be put under special surveillance
     *
     * @param member
     * @return
     */
    public boolean hasSpecialSurveillance(Member member) {
        return member.hasSpecialSurveillance();
    }
    
    /**
     * Adds a new henchman to a boss' list.
     * The member is added to the list in order (joined date
     * in the family)
     *
     * @param boss
     * @param henchman
     */
    private void addHenchman(Member boss, Member member) {
        List<Member> henchmen = boss.getHenchmen();
        if (henchmen != null) {
            int position = getPosition(henchmen, member);
            henchmen.add(position, member);
        }
    }

    /**
     * Gets the position to insert the member into the list
     *
     * @param henchmen
     * @param member
     * @return
     */
    private int getPosition(List<Member> henchmen, Member member) {
        int index = 0;
        for (index = 0; index < henchmen.size(); index++) {
            Member item = henchmen.get(index);
            if (item.getJoined().compareTo(member.getJoined()) > 0) {
                break;
            }
        }
        return index;
    }
}
